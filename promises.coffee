someAsyncMethod()
	.then (result) ->
		DoSomethingSynchronous()
		return someOtherAsyncMethod("stuff")
	.then (result) ->
		return yetAnotherAsyncMethod("things", "and stuff")
	.then (result) ->
		console.log "Whoo, we're done!"
		exit 0
	